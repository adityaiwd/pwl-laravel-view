@extends('layouts/main')

@section('title', 'Welcome')

@section('container')
    <div class="container">
        <div class="row gy-3 mt-5">
            @for ($i = 0; $i < 16; $i++)
                <div class="col-md">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                                the card's content.</p>
                            <a href="#" class="card-link">Card link</a>
                            <a href="#" class="card-link">Another link</a>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
@endsection
