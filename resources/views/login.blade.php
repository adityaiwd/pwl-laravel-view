@extends('layouts/form')
@section('title', 'Login')

@section('form')
    <h1 class="mb-5">Welcome Back!</h1>
    <form style="width:100%;" action="#">
        <div class="mb-3">
            <input type="text" class="form-control form-input" id="Username" placeholder="Username">
        </div>
        <div class="mb-3">
            <input type="password" class="form-control form-input" id="Password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary btn-form mt-3 mb-4">Sign In</button>
    </form>
    <a href="/register" class="link-primary link-form">Register</a>
@endsection
