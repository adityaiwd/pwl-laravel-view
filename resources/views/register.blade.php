@extends('layouts/form')

@section('title',"Register")

@section('form')
<h1 class="mb-5">Don't Have an Account? <br> Register Now!</h1>
    <form style="width:100%;" action="#">
        <div class="mb-3">
            <input type="email" class="form-control form-input" id="Email" placeholder="Email">
        </div>
        <div class="mb-3">
            <input type="text" class="form-control form-input" id="Username" placeholder="Username">
        </div>
        <div class="mb-3">
            <input type="password" class="form-control form-input" id="Password" placeholder="Password">
        </div>
        <div class="mb-3">
            <input type="password" class="form-control form-input" id="ConfirmPassword" placeholder="Confirm Password">
        </div>
        <button type="submit" class="btn btn-primary btn-form mt-3 mb-4">Register</button>
    </form>
    <a href="/login" class="link-primary link-form">Sign In</a>
@endsection
