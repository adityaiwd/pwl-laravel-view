@extends('layouts.main')
@section('title', 'Login')

@section('container')
    <div class="container">
        <div class="formview">
            <div class="formwrapper">
                @yield('form')
            </div>
        </div>
    </div>
@endsection
